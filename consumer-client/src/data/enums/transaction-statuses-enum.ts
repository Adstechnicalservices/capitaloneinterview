

export enum TransactionStatus {
    Pending = "Pending",
    Completed = "Completed",
    Rejected = "Rejected"
}