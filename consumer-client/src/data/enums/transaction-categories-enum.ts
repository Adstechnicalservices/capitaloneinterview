

export enum TransactionCategory {
    Grocery = "Grocery",
    Restaurants = "Restaurants",
    Uncategorized = "Uncategorized"
}