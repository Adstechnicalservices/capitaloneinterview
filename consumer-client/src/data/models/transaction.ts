import { TransactionStatus } from "../enums/transaction-statuses-enum";
import { TransactionCategory } from "../enums/transaction-categories-enum";

export interface Transaction {
    id: string;
    accountId: string;
    transactionDate: Date;
    status: TransactionStatus;
    category: TransactionCategory;
}