import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankAccountTransferPage } from './bank-account-transfer';

@NgModule({
  declarations: [
    BankAccountTransferPage,
  ],
  imports: [
    IonicPageModule.forChild(BankAccountTransferPage),
  ],
})
export class BankAccountTransferPageModule {}
