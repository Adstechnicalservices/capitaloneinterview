import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Transfer } from '../../data/models/transfer';
import { AppDataProvider } from '../../providers/app-data/app-data';

/**
 * Generated class for the MyTransfersPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-transfers',
  templateUrl: 'my-transfers.html',
})
export class MyTransfersPage {
  public transfers: Transfer[] = [];
  constructor(public navCtrl: NavController, public navParams: NavParams, public appDataProvider: AppDataProvider) {
  }

  ngOnInit() {
    setTimeout(() => {
        this.appDataProvider.get().accounts.forEach(account => {

        if (account.transfers && account.transfers.length > 0) {

          this.transfers.push(...account.transfers);
        }
      })   
    }, 200);


    
  }
  ionViewDidLoad() {
    console.log('ionViewDidLoad MyTransfersPage');
  }

}
