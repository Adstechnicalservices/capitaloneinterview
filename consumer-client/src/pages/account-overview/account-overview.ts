import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { BankAccount } from '../../data/models/bank-account';
import { GenericHttpProvider } from '../../providers/generic-http/generic-http';
import { gatewayApiUrl } from '../../app/app.constants';
import { AppDataProvider } from '../../providers/app-data/app-data';

/**
 * Generated class for the AccountOverviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-account-overview',
  templateUrl: 'account-overview.html',
})
export class AccountOverviewPage {
 feed = 'FeedPage';
 myAccounts = 'MyAccountsPage';
 myTransfers = 'MyTransfersPage';
 myDisputes = 'MyDisputesPage';
 help = 'HelpPage';
  constructor(public navCtrl: NavController, public navParams: NavParams, private http: GenericHttpProvider, private appData: AppDataProvider) {
  }

  ngOnInit() {
    console.log('IN NG ON INIT')
    this.http.get<BankAccount[]>(gatewayApiUrl + '/consumer-banking/accounts').toPromise().then(accounts => {
      console.log('Got back accounts');
      if(accounts && accounts.length > 0) {
        this.appData.modifyAccounts(accounts);
        console.log(accounts);
      }
      
    }, error => {
      console.log('error');
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad AccountOverviewPage');
  }

  logout() {
    localStorage.removeItem('accessToken');
    this.navCtrl.setRoot('LoginPage');
  }

}
