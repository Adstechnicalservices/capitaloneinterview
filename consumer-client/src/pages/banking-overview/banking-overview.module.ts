import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankingOverviewPage } from './banking-overview';

@NgModule({
  declarations: [
    BankingOverviewPage,
  ],
  imports: [
    IonicPageModule.forChild(BankingOverviewPage),
  ],
})
export class BankingOverviewPageModule {}
