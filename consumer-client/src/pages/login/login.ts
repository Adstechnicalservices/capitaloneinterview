import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { GenericHttpProvider } from '../../providers/generic-http/generic-http';
import { gatewayApiUrl } from '../../app/app.constants';
import { HttpErrorResponse } from '@angular/common/http';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  email: string;
  password: string;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams, 
    public toastCtrl: ToastController, 
    public loadingCtrl: LoadingController,
    private http: GenericHttpProvider) {
  }

  ngOnInit() {
    const token = localStorage.getItem('accessToken');
    if (token) {
      this.navCtrl.setRoot('AccountOverviewPage');
    }
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
  }

   logIn() {
    if (!this.email || !this.password) {
      this.toastCtrl.create({ message: 'Email and password are required.', duration: 3000  }).present();
    } else {
      // Log in
      const loading = this.loadingCtrl.create({
        content: 'Logging in...'
      });
      loading.present();
      this.http.post<{ email: string; password: string }, { token: string }>(gatewayApiUrl + '/log-in', {
        email: this.email,
        password: this.password
      }).subscribe((result: { token: string }) => {
        loading.dismiss();
        localStorage.setItem('accessToken', result.token);
        this.navCtrl.setRoot('AccountOverviewPage');
      }, (errorResponse: HttpErrorResponse) => {
        console.log(errorResponse.error);
        console.log(errorResponse.message);
        const error = errorResponse.error.message ? errorResponse.error.message : 'There was a system error logging in.';
        loading.dismiss();
        this.toastCtrl.create({ message: error, duration: 3000 }).present();
      });
      
    }
  }

}
