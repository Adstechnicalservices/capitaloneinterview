import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { GenericHttpProvider } from '../../providers/generic-http/generic-http';
import { BankAccount } from '../../data/models/bank-account';
import { gatewayApiUrl } from '../../app/app.constants';
import { AppDataProvider } from '../../providers/app-data/app-data';

/**
 * Generated class for the MyAccountsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-my-accounts',
  templateUrl: 'my-accounts.html',
})
export class MyAccountsPage {
  public myAccounts: BankAccount[] = [];

  constructor(public navCtrl: NavController, public navParams: NavParams, private appData: AppDataProvider) {
  }

  ngOnInit() {
    console.log('NgOnInit My Accounts');
    setTimeout(() => {
      this.myAccounts = this.appData.get().accounts;
    }, 200);
      
    
    
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MyAccountsPage');
  }

}
