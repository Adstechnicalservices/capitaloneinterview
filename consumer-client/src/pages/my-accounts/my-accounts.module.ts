import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyAccountsPage } from './my-accounts';

@NgModule({
  declarations: [
    MyAccountsPage,
  ],
  imports: [
    IonicPageModule.forChild(MyAccountsPage),
  ],
})
export class MyAccountsPageModule {}
