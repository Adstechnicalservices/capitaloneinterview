import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MyDisputesPage } from './my-disputes';

@NgModule({
  declarations: [
    MyDisputesPage,
  ],
  imports: [
    IonicPageModule.forChild(MyDisputesPage),
  ],
})
export class MyDisputesPageModule {}
