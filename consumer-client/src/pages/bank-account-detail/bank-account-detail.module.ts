import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BankAccountDetailPage } from './bank-account-detail';

@NgModule({
  declarations: [
    BankAccountDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(BankAccountDetailPage),
  ],
})
export class BankAccountDetailPageModule {}
