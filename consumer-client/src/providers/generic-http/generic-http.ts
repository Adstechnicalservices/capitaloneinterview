import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

/*
  Generated class for the GenericHttpProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class GenericHttpProvider {

  constructor(public http: HttpClient) {
    console.log('Hello GenericHttpProvider Provider');
  }

  private createAuthHeader(): { accessToken?: string } {
    const token = localStorage.getItem('accessToken');
    if (token) {
      return {
        accessToken: token
      }
    } else {
      return {};
    }
  }

   get<T>(url: string) {
     console.log('Url ' + url);
    return this.http.get<T>(url, {
      headers: this.createAuthHeader()
    });
  }

   post<S, T>(url: string, data: S) {
    const token = this.createAuthHeader();
      return this.http.post<T>(url, data,  {
        headers:  token
      });

  }

   put<S, T>(url: string, data: S) {
    return this.http.put<T>(url, data, {
      headers:  this.createAuthHeader()
    });
  }

   delete<T>(url: string) {
    return this.http.delete<T>(url, {
      headers:  this.createAuthHeader()
    });
  }

}
