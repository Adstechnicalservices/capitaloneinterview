
import { Injectable } from '@angular/core';
import { AppDataModel } from './app-data-model';
import { BankAccount } from '../../data/models/bank-account';
import { Subject } from 'rxjs/Subject';

/*
  Generated class for the AppDataProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class AppDataProvider {
  private appData: AppDataModel = {
    accounts: [] 
  };
  // private appDataSubj: Subject<AppDataModel> = new Subject();

  get() {
    return this.appData;
  }

  modifyAccounts(data: BankAccount[]) {
    this.appData.accounts = data;
   // this.appDataSubj.next(this.appData);
  }

}
