import { BankAccount } from "../../data/models/bank-account";


export interface AppDataModel {
    accounts: BankAccount[];
}