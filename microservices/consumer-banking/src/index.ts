import express from 'express';
import socketClient from 'socket.io-client';
import { getRandomPort } from './helpers';
import { appRoutes } from './routing';
import { MicroserviceInstance } from './deployment/models/microservice-instance';

const app = express();
// prefix for all routes
app.use('/consumer-banking', appRoutes);

const server = app.listen(getRandomPort());

// To dynamically register with the gateway
const microserviceRegistration = socketClient('http://localhost:3000');
microserviceRegistration.on('connect', () => {
    console.log('connected');
});

// Gateway sends back when it registers the instance
microserviceRegistration.on('registered', () => {
    console.log('Registered instance of microservice at ' + server.address().port + '.');
});

const instance: MicroserviceInstance = {
    microserviceName: 'Consumer Banking',
    port: server.address().port,
    version: '1.0.0',
    host: 'localhost',
    scheme: 'http'
};

// This makes microservice available via the gateway proxy service
microserviceRegistration.emit('registering', instance);