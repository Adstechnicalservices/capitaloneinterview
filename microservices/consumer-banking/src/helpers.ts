

export const getRandomPort = () => {
    const minPort = Math.ceil(49152);
    const maxPort = Math.floor(65535);
    return Math.floor(Math.random() * (maxPort - minPort)) + minPort;
  }