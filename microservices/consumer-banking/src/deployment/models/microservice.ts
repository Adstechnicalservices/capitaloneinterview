import { MicroserviceInstance } from "./microservice-instance";


export interface Microservice {
    name: string;
    pathPrefix: string;
    description: string;
    instances?: MicroserviceInstance[];
}