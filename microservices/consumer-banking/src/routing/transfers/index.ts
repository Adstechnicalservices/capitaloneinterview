import {Router} from 'express';
import { getAllTransfers, getTransferDetails, makeTransfer, cancelTransfer } from '../../logic/transfers';

const router =  Router();

// consumer-banking/transfers
router.get('/', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            })
        } else {
            const transfers = await getAllTransfers(userId);
            res.json(transfers);
        }
    } catch(error) {
        res.status(500).json({
            error: 'There was an internal error.'
        });
    }
});

// consumer-banking/transfers/transfer-details/1234
router.get('/transfer-details/:transferId', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            })
        } else {
            const details = await getTransferDetails(userId, req.params.transferId);
            res.json(details);
        }
        
    } catch(error) {
        // TODO
    }
});

// consumer-banking/transfers/make-transfer
router.post('/make-transfer', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            })
        } else {
            await makeTransfer(userId, req.body);
            res.json({});
        }
        
    } catch(error) {
        // TODO
    }
});

// consumer-banking/transfers/cancel-transfer
router.put('/cancel-transfer/:id', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            })
        } else {
            await cancelTransfer(userId, req.params.id);
            res.json({});
        }
        
    } catch(error) {
        // TODO
    }
});

export { router as transferRoutes }