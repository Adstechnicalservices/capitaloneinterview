import { Router } from "express";
import { accountRoutes } from "./accounts";
import { transferRoutes } from "./transfers";
import { transactionRoutes } from "./transactions";


const router = Router();

router.use('/accounts', accountRoutes);
router.use('/transfers', transferRoutes);
router.use('/transactions', transactionRoutes);

export { router as appRoutes };