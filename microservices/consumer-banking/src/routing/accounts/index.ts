import {Router} from 'express';
import { BankAccount } from '../../data/models/bank-account';
import { AccountTypes } from '../../data/enums/account-types-enum';
import { getAccountDetail, getAllAccounts, deleteAccount } from '../../logic/accounts';

const router =  Router();

// consumer-banking/accounts
router.get('/', async (req, res) => {
    try {
        console.log('In the route');
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            })
        } else {
            const accounts = await getAllAccounts(userId);
            res.json(accounts);
        }
        
    } catch(error) {
        console.log(error);
        res.json({
            error: 'There is an error getting the accounts'
        })
    }
    
});

// consumer-banking/accounts/account-overview/1234
router.get('/account-overview/:accountId', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            });
        } else {
            const account = await getAccountDetail(userId, req.params.accountId);
            res.json(account);
        }
    } catch(error) {
        // TODO
    }
});

// consumer-banking/accounts/delete-account/1234
router.delete('/delete-account/:accountId', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            })
        } else {
            await deleteAccount(userId, req.params.accountId);
            res.json({});
        }
    } catch(error) {
        // TODO
    }
});

export { router as accountRoutes };