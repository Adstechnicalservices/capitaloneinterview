import {Router} from 'express';
import { getAllTransactions, disputeTransaction, starTransaction } from '../../logic/transactions';

const router =  Router();

// consumer-banking/transactions
router.get('/', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            })
        } else {
            const transactions = await getAllTransactions(userId);
            res.json(transactions);
        }

    } catch(error) {
        // TODO
    }
});

// consumer-banking/transactions/dispute-transaction/1234
router.put('/dispute-transaction/:transactionId', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            });
        } else {
            await disputeTransaction(userId, req.params.transactionId);
            res.json({});
        }
    } catch (error) {
        // TODO
    }
});

// consumer-banking/transactions/star-transaction/1234
router.put('/star-transaction/:transactionId', async (req, res) => {
    try {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json({
                message: 'Unauthorized'
            });
        } else {
            await starTransaction(userId, req.params.transactionId);
            res.json({});
        }
    } catch(error) {
        // TODO
    }
});

export { router as transactionRoutes };