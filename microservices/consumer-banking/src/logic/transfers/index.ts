import axios from 'axios';
import { Transfer } from '../../data/models/transfer';

export const makeTransfer = async (userId: string, transfer: Transfer) => {
    const response = await  axios.post<Transfer>('http://localhost:5000/api/transfers/make-transfer', transfer, {
        headers: {
            userId: userId
        }
    });
    return response.data;
}

export const cancelTransfer = async (userId: string, transferId: string) => {
    const response = await  axios.put<Transfer>('http://localhost:5000/api/transfers/cancel-transfer/' + transferId, {
        headers: {
            userId: userId
        }
    });
    return response.data;
}

export const getAllTransfers = async (userId: string) => {
    const response = await  axios.get<Transfer>('http://localhost:5000/api/transfers/', {
        headers: {
            userId: userId
        }
    });
    return response.data;
}

export const getTransferDetails = async (userId: string, transferId: string) => {
    const response = await  axios.get<Transfer>('http://localhost:5000/api/transfers/' + transferId, {
        headers: {
            userId: userId
        }
    });
    return response.data;
}