import axios from 'axios';
import { AccountTypes } from "../../data/enums/account-types-enum";
import { BankAccount } from "../../data/models/bank-account";


export const getAllAccounts = async (userId: string) => {
  const response = await  axios.get<BankAccount[]>('http://localhost:5000/api/accounts', {
      headers: {
          userId: userId
      }
  });
  console.log(response.statusText);
  return response.data;
}

export const getAccountDetail = async (userId: string, accountId: string) => {
    const response = await  axios.get<BankAccount>('http://localhost:5000/api/accounts/' + accountId, {
        headers: {
            userId: userId
        }
    });
    return response.data;
}

export const deleteAccount = async (userId: string, accountId: string) => {
    const response = await  axios.delete('http://localhost:5000/api/accounts/' + accountId, {
        headers: {
            userId: userId
        }
    });
    return response.data;
}