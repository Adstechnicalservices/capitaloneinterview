import axios from 'axios';
import { Transaction } from '../../data/models/transaction';

export const getAllTransactions = async (userId: string) => {
    const response = await  axios.get<Transaction>('http://localhost:5000/api/transactions/', {
        headers: {
            userId: userId
        }
    });
    return response.data;
}

export const getTransactionDetail = async (userId: string, transactionId: string) => {
    const response = await  axios.get<Transaction>('http://localhost:5000/api/transactions/' + transactionId, {
        headers: {
            userId: userId
        }
    });
    return response.data;
}

export const disputeTransaction = async (userId: string, transactionId: string) => {
    const response = await  axios.put<Transaction>('http://localhost:5000/api/transactions/dispute-transaction' + transactionId, {
        headers: {
            userId: userId
        }
    });
    return response.data;
}

export const starTransaction = async (userId: string, transactionId: string) => {
    const response = await  axios.put<Transaction>('http://localhost:5000/api/transactions/star-transaction' + transactionId, {
        userId: userId
    });
    return response.data;
}