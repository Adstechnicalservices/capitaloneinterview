import { Microservice } from "./microservice";


export interface MicroserviceInstance {
    id?: string;
    microserviceName: string;
    version: string;
    scheme: 'http' | 'https';
    host: string;
    port: number;
}