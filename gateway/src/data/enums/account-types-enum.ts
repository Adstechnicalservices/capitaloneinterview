export enum AccountTypes {
    Checking = "Checking",
    Savings = "Savings"
}