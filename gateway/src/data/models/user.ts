

export interface User {
    id: string;
    firstName: string;
    lastName: string;
    ssn: number;
    email: string;
}