import { AccountTypes } from "../enums/account-types-enum";
import { Transfer } from "./transfer";
import { Transaction } from "./transaction";

export interface BankAccount {
    id: string;
    userId: string;
    accountNumber: number;
    accountType: AccountTypes;
    balance: number;
    transfers: Transfer[];
    transactions: Transaction[];
}