import { Payee } from "./payee";


export interface Transfer {
    id: string;
    accountId: string;
    amount: number;
    created: Date;
    payee: Payee;
}