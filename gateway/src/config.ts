import { Microservice } from "./deployment/models/microservice";
import { v4 } from "uuid";

interface GatewayConfig {
    jwtSecret: string;
    microservices: {
        consumerBanking: Microservice
    }
}

export const config: GatewayConfig = {
    jwtSecret: 'ASecretKey111!',
    microservices: {
        consumerBanking: {
            name: 'Consumer Banking',
            description: 'This is all of the consumer banking',
            pathPrefix: '/consumer-banking'
        }
    }
}