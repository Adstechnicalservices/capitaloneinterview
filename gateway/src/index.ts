import express from 'express';
import cors from 'cors';
import helmet from 'helmet';
import bodyParser from 'body-parser';
import socket from 'socket.io';
import _ from 'lodash';
import axios from 'axios';
import httpProxy from 'http-proxy';
import { authenticationMiddleware } from './middleware/authentication';
import { MicroserviceInstance } from './deployment/models/microservice-instance';
import { config } from './config';

const app = express();
const proxy = httpProxy.createProxyServer({
});
const microservices = config.microservices;


app.use(helmet());
app.use(cors());
app.use(bodyParser.json());
app.use(authenticationMiddleware);
app.use((req, res, next) => {
    console.log(res.locals);
    if (req.path.startsWith('/consumer-banking')) {
        if (!microservices.consumerBanking.instances) {
            res.status(500).json({
                error: 'The Consumer Banking service is not running.'
            });
        } else {
            const instance = _.sample(microservices.consumerBanking.instances);
            if (!instance) {
                res.status(500).json({
                    error: 'The Consumer Banking service is not running.'
                });
            } else {
                console.log('There is an instance');
                const userId = res.locals.userId;
                proxy.web(req, res, {
                    // http://localhost:45693
                    target: instance.scheme + '://' + instance.host + ':' + instance.port,
                    headers: {
                        userId: userId
                    }
                });
            }
        }
    }
});

const server = app.listen(3000);
console.log('Listening on ' + server.address().port);
const microserviceHandler = socket(server);

microserviceHandler.on('connection', socket => {
    socket.on('registering', (microserviceInstance: MicroserviceInstance) => {
        const microservice = config.microservices.consumerBanking;//find(microservice => microservice.name == microserviceInstance.microserviceName);
        if (microservice) {
            console.log('Registering ' + microservice.name + ' service on port ' + microserviceInstance.port);
            if (microservice.instances) {
                microservice.instances.push(microserviceInstance);
            } else {
                microservice.instances = [
                    microserviceInstance
                ];
            }
            socket.emit('registered');
        } else {
            console.log('Failed to register.  Unknown microservice.');
        }

    });
});