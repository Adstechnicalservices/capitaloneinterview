import { NextFunction, Response, Request } from "express";
import jwt, { sign } from 'jsonwebtoken';

import axios from 'axios';
import { config } from "../../config";
import { TokenClaims } from "./token-claims";
import { SignInVm } from "./sign-in-vm";
import { User } from "../../data/models/user";

// Client authentication
export const authenticationMiddleware = async (req: Request, res: Response, next: NextFunction) => {
    console.log(req.path);

    if (req.path == '/log-in' && req.method == 'POST') {
        console.log('Yep');
        const signInVm: SignInVm = req.body;
        if (!signInVm.email || !signInVm.password) {
            res.status(400).send({
                message: 'Email and password are required.'
            })
        } else {
            console.log('Finding user by email ' + signInVm.email);
            const response = await axios.get<User>('http://localhost:5000/api/users/findUserByEmail/' + signInVm.email);
            console.log(response.data);
            if (!response.data.id) {
                res.status(401).json({
                    message: 'We could not locate a user with that email and password.'
                })
            } else {
                res.send({
                    token: jwt.sign({
                        userId: response.data.id
                    }, config.jwtSecret)
                });
            }
        }
    } else {
        const accessToken = req.header('accessToken');
        if (!accessToken) {
            res.status(401).json({
                message: 'Unauthorized'
            })
        } else {
            try {
                const decoded: TokenClaims = jwt.verify(accessToken, config.jwtSecret) as TokenClaims;
                console.log('Decoded ');
                console.log(decoded);
                console.log(decoded.userId);
                res.locals.userId = decoded.userId;
                next();
            } catch (error) {
                res.status(401).json({
                    message: 'Unauthorized'
                });
            }
        }
    }
}