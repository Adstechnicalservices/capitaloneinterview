import express from 'express';
import { initializeDatabase, db, DbCollections } from './initialize-database';
import { Transfer } from './data/models/transfer';

const runApp = async() => {
    await initializeDatabase(true);
    db.subscribe(db => {
        startApp(db);
    })
}

const startApp = async(db: DbCollections) => {
    const app = express();
    const router = express.Router();
    router.get('/users', (req, res) => {
        res.json(db.Users.find({}));
    });
    router.get('/users/findUserByEmail/:email', (req, res) => {
        res.json(db.Users.find({ email: req.params.email })[0]);
    });
    router.get('/accounts', (req, res) => {
        const userId = req.header('userId');
        console.log('User id is ' + userId);
        console.log(res.locals);
        if(!userId) {
            res.json([]);
        } else {
            res.json(db.BankAccounts.find({ userId: userId }));
        }
        
    });
    router.get('/accounts/:id', (req, res) => {

        const userId = req.header('userId');
        const accountId = req.params.id;
        if(!userId || !accountId) {
            res.json({});
        } else {
            res.json(db.BankAccounts.find({ userId: userId, id: accountId })[0]);
        }
    });
    router.delete('/accounts/:id', (req, res) => {
        const userId = req.header('userId');
        const accountId = req.params.id;
        if (!userId || !accountId) {
            res.status(400).json({});
        } else {
            res.json(db.BankAccounts.findAndRemove({ userId: userId, id: req.params.id }));
        }
    });
    router.get('/transfers', (req, res) => {
        const userId = req.header('userId');
        if (!userId) {
            res.status(401).json([]);
        } else {
            res.json(db.Transfers.find());
        }
        
    });
    router.get('/transfers/:id', async (req, res) => {
        const userId = req.header('userId');
        const transferId = req.params.id;
        if (!userId || !transferId) {
            res.status(401).json({});
        } else {
            const transfer = db.Transfers.find({  id: transferId })[0];
            if(transfer) {
               const account = db.BankAccounts.find({ userId: userId, id: transfer.id });
               if (account) {
                  return res.json(transfer);
               }
            }
            
            res.status(400).json({});
        }
    });
    router.post('/transfers/make-transfer', (req, res) => {
        res.json(db.Transfers.insert(req.body as Transfer));
    });
    // router.get('/transactions', (req, res) => {
    //     const userId = req.header('userId');
    //     if (!userId) {
    //         res.status(401).json({});
    //     } else {

    //         res.json(db.Transactions.find({ accountId}));
    //     }
        
    // });
    router.get('/transactions/:id', (req, res) => {
        res.json(db.Transactions.find({ id: req.params.id })[0]);
    });
    app.use('/api', router);
    app.listen(5000);
}


runApp();