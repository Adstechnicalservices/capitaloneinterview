export interface Payee {
    id: string;
    userId: string;
    nickname: string;
    accountNumber: number;
    routingNumber: number;
}