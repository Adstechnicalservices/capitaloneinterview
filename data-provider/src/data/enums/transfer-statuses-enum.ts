

export enum TransferStatus {
    Initiated = "Initiated",
    NegotiatingWithPayeeBank = "Negotiating With Payee Bank",
    AcceptedByPayeeBank = "Accepted By Payee Bank",
    Completed = "Completed",
    Failed = "Failed"
}