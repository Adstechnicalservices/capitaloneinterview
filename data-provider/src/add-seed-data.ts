import { v4 } from 'uuid';
import { User } from "./data/models/user";
import { BankAccount } from "./data/models/bank-account";
import { Transaction } from "./data/models/transaction";
import { Transfer } from "./data/models/transfer";
import { Payee } from "./data/models/payee";
import { AccountTypes } from './data/enums/account-types-enum';
import { DbCollections } from './initialize-database';
import {  createRandomUser, createRandomBankAccount, createRandomPayee, createRandomTransaction, createRandomTransfer } from './helpers';


export const addSeedData = async (dbCollections: DbCollections, numUsers: number) => {

    // First create a user, then a payee and an account for the user.  Then insert random transactions and transfers to the payee

    for (let i = 0; i < numUsers; i++) {
        const user =  dbCollections.Users.insert(createRandomUser());
        console.log(user);
        if (user) {
            const payee = createRandomPayee(user.id);
            dbCollections.Payees.insert(payee);
            const account = dbCollections.BankAccounts.insert(createRandomBankAccount(user.id, payee, 15, 3));
            // if (account) {
            //     for (let i=0; i < 5; i++) {
            //         dbCollections.Transactions.insert(createRandomTransaction(account.id));
            //         if (payee) {
            //             dbCollections.Transfers.insert(createRandomTransfer(account.id, payee));
            //         }                    
            //     }
            // }
        }
        
    }

    return dbCollections;

}