import lokijs from 'lokijs';
import {Observable, Subject} from 'rxjs';
import { BankAccount } from './data/models/bank-account';
import { User } from './data/models/user';
import { Transaction } from './data/models/transaction';
import { Transfer } from './data/models/transfer';
import {  addSeedData } from './add-seed-data';
import { Payee } from './data/models/payee';
const lokiDb = new lokijs('db.json');

export interface DbCollections {
    Users: Collection<User>;
     BankAccounts: Collection<BankAccount>;
     Transactions: Collection<Transaction>;
     Payees: Collection<Payee>;
     Transfers: Collection<Transfer>;
}

const dbSubject = new Subject<DbCollections>();
export const db = dbSubject.asObservable();

export const initializeDatabase = (reset?: boolean) => {
    if (reset) {
        lokiDb.collections.forEach(collection => {
            lokiDb.removeCollection(collection.name);
        });
    }
    let Users: Collection<User> = lokiDb.getCollection('users');
    let BankAccounts: Collection<BankAccount> = lokiDb.getCollection('bank-accounts');
    let Transactions: Collection<Transaction> = lokiDb.getCollection('transactions');
    let Payees: Collection<Payee> = lokiDb.getCollection('payees');
    let Transfers: Collection<Transfer> = lokiDb.getCollection('transfers');


    lokiDb.loadDatabase({}, async err => {
        if (err) {
            return;
        }

        if (!Users) {
            Users = lokiDb.addCollection<User>('users');
         }   

         if (!BankAccounts) {
             BankAccounts = lokiDb.addCollection<BankAccount>('bank-accounts');
         }

         if (!Transactions) {
             Transactions =  lokiDb.addCollection<Transaction>('transactions');
         }

         if (!Payees) {
             Payees = lokiDb.addCollection<Payee>('payees');
         }

         if (!Transfers) {
            Transfers =  lokiDb.addCollection<Transfer>('transfers');
         }
         console.log(lokiDb.listCollections().length);
         lokiDb.saveDatabase();
         let dbCollections: DbCollections = {
             Users: Users,
             BankAccounts: BankAccounts,
             Transactions: Transactions,
             Transfers: Transfers,
             Payees: Payees
         };
         console.log(dbCollections.Users.count());
         if (dbCollections.Users.count() == 0) {
           dbCollections = await addSeedData(dbCollections, 5);
           lokiDb.saveDatabase();
         }         
         dbSubject.next(dbCollections);
    });

}
