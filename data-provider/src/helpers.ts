import { User } from "./data/models/user";
import { v4 } from "uuid";
import _ from 'lodash';
import { BankAccount } from "./data/models/bank-account";
import { AccountTypes } from "./data/enums/account-types-enum";
import { Transaction } from "./data/models/transaction";
import { TransactionStatus } from "./data/enums/transaction-statuses-enum";
import { TransactionCategory } from "./data/enums/transaction-categories-enum";
import { Transfer } from "./data/models/transfer";
import { Payee } from "./data/models/payee";



const firstNames = [ 'John', 'Mary', 'Jane', 'Harry', 'Larry', 'Lewis', 'Max'];

const lastNames = ['Smith', 'Hall', 'Hamilton', 'Verstappen', 'Sanchez', 'Gates'];

 const generateAccountNumber = () => {
    const minAccountNumber = Math.ceil(10000);
    const maxAccountNumber = Math.floor(999999);
    return Math.floor(Math.random() * (maxAccountNumber - minAccountNumber)) + minAccountNumber;
}

 const generateRandomCurrency = (minInput: number, maxInput: number) => {
    const min = Math.ceil(minInput);
    const max = Math.floor(maxInput);
    return Math.floor(Math.random() * (max - min)) + min;
}

 const generateRandomSsn = () => {
    const min = Math.ceil(1000000);
    const max = Math.floor(99999999);
    return Math.floor(Math.random() * (max - min)) + min;
}

const generateRandomAccountType = () => {
    if (Date.now() % 2 == 0) {
        return AccountTypes.Checking
    } else {
        return AccountTypes.Savings
    }
}

const generateRandomTransactionStatus = () => {
    const now = Date.now().toString();
    const number = parseInt( now.slice(now.length -1));
    console.log('Num ' + number);
    switch(number) {
        case 0 :
        case 1:
        case 2: 
          return TransactionStatus.Completed;
        case 4:
        case 5:
        case 6:
            return TransactionStatus.Pending;
        case 7:
        case 8:
        case 9:
            return TransactionStatus.Rejected;
    }
    return TransactionStatus.Pending;
}

export const createRandomUser = () : User => {
    const firstName = _.sample(firstNames) || 'John';
    const lastName = _.sample(lastNames) || 'Smith';
    return {
        id: v4(),
        firstName: firstName,
        lastName: lastName,
        email: firstName + '.' + lastName + '@gmail.com',
        ssn: generateRandomSsn()
    }
}

export const createRandomBankAccount = (userId: string, randomPayee: Payee, numTransactions: number, numTransfers: number) : BankAccount => {
    const id = v4();
    let transactions: Transaction[] = [];
    let transfers: Transfer[] = [];
    for (let i = 0; i < numTransactions; i++ ) {
        transactions.push(createRandomTransaction(id))
    }
    for (let i = 0; i < numTransfers; i++ ) {
        transfers.push(createRandomTransfer(id, randomPayee))
    }
    return {
        id: id,
        userId: userId,
        balance: generateRandomCurrency(100, 1000000),
        accountNumber: generateAccountNumber(),
        accountType: generateRandomAccountType(),
        transactions: transactions,
        transfers: transfers
    }
}

export const createRandomTransaction = (accountId: string): Transaction => {
    return {
        id: v4(),
        accountId: accountId,
        status: generateRandomTransactionStatus(),
        transactionDate: new Date(),
        category: TransactionCategory.Grocery
    }
}

export const createRandomPayee = (userId: string) : Payee => {
    return {
        id: v4(),
        userId: userId,
        accountNumber: generateAccountNumber(),
        routingNumber: 38383939,
        nickname: 'IOU'
    }
}

export const createRandomTransfer = (accountId: string, payee: Payee) : Transfer => {
    return {
        id: v4(),
        accountId: accountId,
        amount: generateRandomCurrency(5, 500),
        created: new Date(),
        payee: payee
    }
}